'use strict';

exports.config = {
  specs: [
    './test/*.js'
  ],
  exclude: [],
  maxInstances: 2, // it depends on the plan of the cloud servvice
  sync: true,
  logLevel: 'error',
  coloredLogs: true,
  waitforTimeout: 20000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  framework: 'mocha',
  reporters: ['spec','junit'],
  reporterOptions: {
    junit: {
      outputDir: './report'
    }
  },
  mochaOpts: {
    ui: 'bdd',
    compilers: ['js:./node_modules/babel-register'],
    timeout: 999999999
  }
};